/*global cordova, module*/

module.exports = {
    withOptions: function (successCallback, errorCallback) {
        cordova.exec(successCallback, errorCallback, "launchDetect", "withOptions");
    }
};
