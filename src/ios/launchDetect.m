#import "launchDetect.h"
#import <UIKit/UIKit.h>

@implementation launchDetect

static NSDictionary* launchOptions;


+(void)load {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didFinishLaunching:)
                                                 name:UIApplicationDidFinishLaunchingNotification
                                               object:nil];
}

+(void)didFinishLaunching:(NSNotification*)notification {
    launchOptions = notification.userInfo; //launchOptions is nil when not start because of notification or url open
}

- (void)withOptions:(CDVInvokedUrlCommand*)command {
    CDVPluginResult* result = [CDVPluginResult
                               resultWithStatus:CDVCommandStatus_OK
                               messageAsDictionary:launchOptions];
    [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
}

@end